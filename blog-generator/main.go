package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/nats_sample/proto"
	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/nats-io/go-nats"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type config struct {
	Addr       string `envconfig:"ADDR" default:":8000"`
	NatsAddr   string `envconfig:"NATS_ADDR" default:"nats://localhost:4222"`
	StaticPath string `envconfig:"STATIC_PATH" default:"./posts"`
}

const (
	topicPublishPost = "post:publish"
)

func main() {
	var cfg config
	if err := envconfig.Process("bloggenerator", &cfg); err != nil {
		log.Fatalf("Failed to load configuration from env: %v", err)
	}

	natsClient, err := nats.Connect(cfg.NatsAddr)
	if err != nil {
		log.Fatalf("Can not connect to %s: %v\n", cfg.NatsAddr, err)
	}

	gen, err := newPageGenerator(cfg.StaticPath)
	if err != nil {
		log.Fatalf("")
	}

	startSubscription(natsClient, topicPublishPost, generatorPostPage(gen))

	r := mux.NewRouter()
	r.PathPrefix("/").Handler(http.FileServer(http.Dir(cfg.StaticPath)))

	log.Infof("Starting HTTP server on '%s'", cfg.Addr)
	if err = http.ListenAndServe(cfg.Addr, r); err != nil {
		log.Fatal(err)
	}
}

func startSubscription(natsClient *nats.Conn, topic string, handler nats.MsgHandler) {
	if _, err := natsClient.Subscribe(topic, handler); err != nil {
		log.Fatalf("Failed to start subscription on '%s': %v", topic, err)
	}

	log.Infof("Started subscription on '%s'", topic)
}

func generatorPostPage(gen pageGenerator) nats.MsgHandler {
	return func(natsMsg *nats.Msg) {
		log.Debug("Received new post generation queue message")

		var message pb.PublishPostMessage
		if err := proto.Unmarshal(natsMsg.Data, &message); err != nil {
			log.Errorf("Failed to unmarshal queue message: %v", err)
			return
		}

		if err := gen.Generate(message); err != nil {
			log.Errorf("Failed to generate post page: %v", err)
		}
	}
}
