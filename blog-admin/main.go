package main

import (
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/nats-io/go-nats"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type config struct {
	Addr     string `envconfig:"ADDR" default:":8000"`
	NatsAddr string `envconfig:"NATS_ADDR" default:"nats://localhost:4222"`
}

func main() {
	var cfg config
	if err := envconfig.Process("blogadmin", &cfg); err != nil {
		log.Fatalf("Failed to load configuration from env: %v", err)
	}

	natsClient, err := nats.Connect(cfg.NatsAddr)
	if err != nil {
		log.Fatalf("Can not connect to %s: %v\n", cfg.NatsAddr, err)
	}

	srv := Server{
		natsClient: natsClient,
	}

	r := mux.NewRouter()
	r.HandleFunc("/publish", srv.HandlePublishPost)

	log.Infof("Starting HTTP server on '%s'", cfg.Addr)
	if err = http.ListenAndServe(cfg.Addr, r); err != nil {
		log.Fatal(err)
	}
}
