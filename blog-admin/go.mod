module bitbucket.org/Dima_Gnatovskiy/nats_sample/blog_admin

require (
	bitbucket.org/Dima_Gnatovskiy/nats_sample/proto v0.0.0-20190226212406-999d40c5ae12
	github.com/golang/protobuf v1.2.0
	github.com/gorilla/mux v1.7.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v1.7.2
	github.com/nats-io/nkeys v0.0.2 // indirect
	github.com/nats-io/nuid v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.3.0
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223 // indirect
)
