module bitbucket.org/Dima_Gnatovskiy/nats_sample

require (
	github.com/golang/protobuf v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)
